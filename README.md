This module forcibly sets all path aliases to 'undefined' language, despite all 
other locale or i18n settings.

Drupal's locale module includes a lot of great features for supporting 
multilingual sites. One such feature is the ability to associate a language 
with a path alias. This allows you to have one node with two versions, let's 
say an English version and a Spanish version. In this case, both the English 
and Spanish versions would have their own path alias.

But your use case may not require language-specific paths per node. Maybe you 
want to call a spade a spade—you've got a Spanish node or and English node and 
that's it. No fancy multiple versions. One node = one path.

Well then you need this module. It will force all path aliases to have an 
'undefined' language, whether the associated node be in English, Spanish, or 
Swahili. This generally simplifies the administrative experience.
